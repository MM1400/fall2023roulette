package code;

import java.util.Scanner;
public class Roulette {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        RouletteWheel roulette = new RouletteWheel();
        int balance = 1000;

        boolean wantBet = wantBet();

        while(balance >= 0) {
            if(wantBet) {
                int bet = makeBet(balance);
                System.out.println("What number would you like to bet on?");
                int number = scan.nextInt();
                roulette.spin();

                System.out.println("The number is " + roulette.getValue());
                if (roulette.getValue() == number) {
                    balance = balance+(bet*35);
                    System.out.println("You have won and your balance is now "+ balance);
                } else {
                    balance -= bet;
                    System.out.println("You have lost and your balance is now "+ balance);
                }
            } else {
                roulette.spin();
                System.out.println("The number is " + roulette.getValue());
            }
            wantBet = wantBet();
        }
    }
    public static int makeBet(int balance) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Bet amount that is equal or below your balance");
        System.out.println("Balance is: " + balance);

        int bet = scan.nextInt();
        while (bet > balance) {
            bet = scan.nextInt();
        }
        return bet;
    }
    public static boolean wantBet() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Would you like to make a new bet: 1 for yes and 2 for no");
        int num = scan.nextInt();
        if(num == 1) {
            return true;
        } 
        return false;
    }
    
}

