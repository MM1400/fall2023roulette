package code;

import java.util.Random;
public class RouletteWheel {

    private Random rand = new Random();
    private int numLast = 0;

    public void spin() {
        this.numLast = rand.nextInt(37);
    }
    public int getValue() {
        return this.numLast;
    }
}
